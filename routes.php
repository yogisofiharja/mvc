<?php
  function call($controller, $action) {
    require_once('controllers/'.$controller.'.php');
    $controller = new $controller();
    $controller->{ $action }();
  }

  $controllers = array(
    'guest' => ['index', 'save', 'guest_list'],
    'error'=> ['index']
    );

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('error', 'index');
    }
  } else {
    call('error', 'index');
  }
?>