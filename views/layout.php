<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MVC</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- Google Webfont -->
  <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700|Unica+One' rel='stylesheet' type='text/css'>
  <!-- Themify Icons -->
  <link rel="stylesheet" href="/mvc/assets/css/themify-icons.css">
  <!-- Icomoon Icons -->
  <link rel="stylesheet" href="/mvc/assets/css/icomoon-icons.css">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="/mvc/assets/css/bootstrap.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="/mvc/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="/mvc/assets/css/owl.theme.default.min.css">
  <!-- Magnific Popup -->
  <link rel="stylesheet" href="/mvc/assets/css/magnific-popup.css">
  <!-- Easy Responsive Tabs -->
  <link rel="stylesheet" href="/mvc/assets/css/easy-responsive-tabs.css">
  <!-- Theme Style -->
  <link rel="stylesheet" href="/mvc/assets/css/style.css">
  <link rel="stylesheet" href="/mvc/assets/css/custom.css">

  
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/modernizr-2.6.2.min.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery -->
  <script src="/mvc/assets/js/jquery-1.10.2.min.js"></script>
</head>
<body>

  <!-- Header -->
  <header id="fh5co-header" role="banner">

    <!-- Logo -->
    <div id="fh5co-logo" class="text-center">
      <a href="?controller=guest&action=index">
        <h2>MVC</h2>
      </a>
    </div>
    <!-- Logo -->

    <!-- Mobile Toggle Menu Button -->
    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
    <!-- Main Nav -->
    <div id="fh5co-main-nav">
      <nav id="fh5co-nav" role="navigation">
        <ul>
          <li class="<?php echo $home = isset($home) ? $home : ''; ?>">
            <a href="/mvc">Home</a>
          </li>
          <li class="<?php echo $guestlist = isset($$guestlist) ? $$guestlist : ''; ?>">
            <a href="/mvc/?controller=guest&action=guest_list">Guest List</a>
          </li>
        </ul>
        <!-- <div class="menu-border"></div> -->
      </nav>
    </div>
    <!-- Main Nav -->
  </header>
  <!-- Header -->

  <!-- Main -->
  <main role="main">

    <?php require_once('routes.php'); ?>
    
  </main>
  <!-- Main -->

  <!-- Footer -->
  <footer id="fh5co-footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p class="fh5co-copyright">&copy; Yogi Sofi Harja</p>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer -->
  
  <!-- Go To Top -->
  <a href="#" class="fh5co-gotop"><i class="ti-shift-left"></i></a>
  

  
  <!-- jQuery Easing -->
  <script src="/mvc/assets/js/jquery.easing.1.3.js"></script>
  <!-- Bootstrap -->
  <script src="/mvc/assets/js/bootstrap.js"></script>
  <!-- Owl carousel -->
  <script src="/mvc/assets/js/owl.carousel.min.js"></script>
  <!-- Magnific Popup -->
  <script src="/mvc/assets/js/jquery.magnific-popup.min.js"></script>
  <!-- Easy Responsive Tabs -->
  <script src="/mvc/assets/js/easyResponsiveTabs.js"></script>
  <!-- FastClick for Mobile/Tablets -->
  <script src="/mvc/assets/js/fastclick.js"></script>
  <!-- Velocity -->
  <script src="/mvc/assets/js/velocity.min.js"></script>
  <!-- Main JS -->
  <script src="/mvc/assets/js/main.js"></script>
  <script type="text/javascript">
      setTimeout(function(){
                $('.alert-success').hide()
              }, 5000);
      /*$('#guest').submit(function(e){
        e.preventDefault();
        $.ajax({
          type : 'POST',
          url : '/mvc/?controller=guest&action=save',
          data : $("#guest :input").serializeArray(),
          dataType : 'json',
          success:function(result){
            console.log(result);
              $("#guest :input").val("");
              $('#submit').after('<div class="alert alert-success" role="alert">Thank you for your comming.</div>');
              setTimeout(function(){
                $('.alert-success').hide()
              }, 5000);
          }
        });

      });*/

  </script>
</body>
</html>
