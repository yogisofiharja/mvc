<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="fh5co-page-title text-center">
				<h1 class="fh5co-title">Guest List</h1>
			</div>
			<div class="fh5co-uppercase-heading-sm">
			<?php
				if(count($guest)<1){
					echo "<h3 class='text-center'>There is no data to show</h3>";
				}else{
					?>
				<table class="table table-striped">
				<thead>
					<tr>
					<th>Name</th>
					<th>Arrival Time</th>
					<th>Email</th>
					<th>Phone</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($guest as $guest) { ?>
					<tr>
						<td><?php echo $guest['name'];?></td>
						<td><?php echo $guest['date'];?></td>
						<td><?php echo $guest['email'];?></td>
						<td><?php echo $guest['phone'];?></td>
					</tr>
					<?php } ?>
				</tbody>
				</table>
					<?php
				}
			?>
			</div>
		</div>
	</div>
</div>