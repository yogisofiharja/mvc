<!-- Form -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="fh5co-uppercase-heading-sm text-center"><strong>Please fill your identity :</strong></h2>
			<!-- <div class="fh5co-spacer fh5co-spacer-xs"></div> -->
		</div>
		<div class="col-md-8 col-md-offset-2">
			<form action="/mvc/?controller=guest&action=save" method="post">
				<div class="col-md-12">
					<div class="form-group">
						<label for="name" class="sr-only">Name</label>
						<input placeholder="Name" id="name" type="text" name="name" class="form-control input-lg" required>
					</div>	
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="email" class="sr-only">Email</label>
						<input placeholder="Email" id="email" type="email" name="email" class="form-control input-lg" required>
					</div>	
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="phone" class="sr-only">Phone</label>
						<input placeholder="phone" id="phone" type="text" name="phone" class="form-control input-lg" required>
					</div>	
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-md" value="Send">
						
					</div>	
				</div>
				<div class="col-md-6">
					<?php 
						if(isset($_GET['result'])){
							if($_GET['result']=="success"){

								?>
								<div class="alert alert-success" role="alert">Thank you for your comming.</div>
								<?php
							}
						}
						?>
				</div>
				
			</form>	
			<div class="fh5co-spacer fh5co-spacer-sm"></div>
		</div>
		
	</div>

</div>