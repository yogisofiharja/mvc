<?php
require_once('connection.php');

	$default_controller = 'guest';
	$default_action = 'index';
	if (isset($_GET['controller']) && isset($_GET['action'])) {
	    $controller = $_GET['controller'];
	    $action     = $_GET['action'];
	  } else {
	    $controller = $default_controller;
	    $action     = $default_action;
	  }

	  require_once('views/layout.php');
?>