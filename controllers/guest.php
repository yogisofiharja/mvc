<?php
  class Guest {

  	public function __construct(){
  		require_once('models/guest_model.php');
  	}
    
    public function guest_list() {
      $guest = Guest_model::all();
      require_once('views/pages/guest/home.php');
    }

    public function index(){
      $home='active';
      require_once('views/pages/guest/add.php');

    }
    public function save(){
    	$guest = Guest_model::save($_POST);

      if($guest == 1){
    	 header('Location:/mvc?controller=guest&action=index&result=success'); 
      }
    }

  }
?>