<?php
class Guest_model {
  public $id;
  public $name;
  public $date;
  public $email;
  public $phone;

  public function __construct($id, $name, $date, $email, $phone) {
    $this->id      = $id;
    $this->name  = $name;
    $this->date  = $date;
    $this->email = $email;
    $this->phone = $phone;

  }

  public static function all() {
    $list = [];
    $db = new Db;
    $db = $db->connect();
      // print_r($db);exit;
    $req = $db->query('SELECT * FROM guest');
    if($req->num_rows>0){
      while ($row = mysqli_fetch_assoc($req)) {
        $list[] = $row;
      }

    }
    return $list;
  }

  public static function save($data){
    $name = $data['name'];
    $email = $data['email'];
    $phone = $data['phone'];
    $db = new Db;
    $db = $db->connect();
    $sql = "insert into guest (name, email, phone) values ('".$name."','".$email."','".$phone."')";
    $result = $db->query($sql);
    return $result;
  }
}
?>